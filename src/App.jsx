import React, { Component } from 'react'

import Componente from './'
import style from './css/index.scss'
import '../test/design/scss/main.scss'

class App extends Component {
  render() {
    return (
      <Componente 
        style={ style }
        title = "Buzios cupos confirmados"
        price={{currency:"ARS", amount:1200, description: "precio final en Base Doble"}}
        oldPrice={{currency:"ARS", amount:1400}}
        services={{flight: true, transport: true, hotel:true, assist_card:true, tickets:true, magic_bands: true, car:true }}
      />
    )
  }
}

export default App
