import React, { Component } from 'react'
import PropTypes            from 'prop-types'
import { Col, Row }         from 'react-bootstrap'

class Card extends Component {

  render() {    

    return (
      <article className={'Card_offer_Component'}>					

			<div>
				
				<Row>
					<Col lg={12}>	

						<div className={'Card_offer_Component_image'}>
							<img src={this.props.imagen}  />	
							{this.props.services && 						
							<ul className="Card_offer_Component_info">			
								{this.props.services.flight && 					
								<li className="ofertas-info-item">
									<i className="icon-item icon-plane"></i>
									vuelo
								</li>				
								}
								{this.props.services.transport && 							
								<li className="ofertas-info-item">
									<i className="icon-item icon-bus"></i>
									traslados
								</li>			
								}		
								{this.props.services.hotel &&		
								<li className="ofertas-info-item">
									<i className="icon-item icon-bell"></i>
									hotel
								</li>
								}			
								{this.props.services.assist_card &&				
								<li className="ofertas-info-item">
									<i className="icon-item icon-assist"></i>
									asistencia
								</li>
								}					
								{this.props.services.tickets &&			
								<li className="ofertas-info-item">
									<i className="icon-item icon-ticket"></i>
									ticket
								</li>	
								}		
								{this.props.services.magic_bands &&				
								<li className="ofertas-info-item">
									<i className="icon-item icon-band"></i>
									magic bands
								</li>		
								}			
								{this.props.services.car &&		
								<li className="ofertas-info-item">
									<i className="icon-item icon-car"></i>
									auto
								</li>
								}
							</ul>	
							}
						</div>
						
					</Col>
				</Row>
				
				<div className="Card_offer_Component_content">
					
					<Row>
						<Col lg={8} md={8} xs={7}> 
							<h2 className="Card_offer_Component_title">
								{this.props.title}
							</h2>
							<div className="Card_offer_Component_description">								
							</div>
						</Col>
						<Col lg={4} md={4} xs={5}> 
							<div className="Card_offer_Component_price">
							{this.props.oldPrice && 
								<p className={'price-old'}>
									<span><i>{this.props.oldPrice.currency}</i>{this.props.oldPrice.amount}</span>	
								</p>
							}
								<p>									
									<b><i>{this.props.price.currency}</i> {this.props.price.amount}</b>	
									<strong>{this.props.price.description}</strong> 
								</p>										
							</div>
						</Col>
					</Row>
						
				</div>
			</div>					

		</article>		
    )
  }
}
Card.propTypes = {
  imagen: PropTypes.string,
  title: PropTypes.string,
	link: PropTypes.string,
	price: PropTypes.string,
	icons: PropTypes.string,
	oldPrice: PropTypes.shape({
    currency: PropTypes.string,
		amount: PropTypes.number
  }), 
	price: PropTypes.shape({
    currency: PropTypes.string,
		amount: PropTypes.number,
		description: PropTypes.string
	}),
	services: PropTypes.shape({
    flight: PropTypes.boolean,
		transport: PropTypes.boolean,
		hotel: PropTypes.boolean,
		assist_card: PropTypes.boolean,
		tickets: PropTypes.boolean,
		magic_bands: PropTypes.boolean,
		car: PropTypes.boolean
	})
}
export default Card
